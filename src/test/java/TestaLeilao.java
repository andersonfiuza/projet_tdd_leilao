import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class TestaLeilao {


    @Test
    public void adiconarNovolance() {

        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Anderson");

        Lance novolance = new Lance(usuario, 200);

        Leilao leilao = new Leilao();


        Assertions.assertEquals(leilao.adicionaNovolance(novolance), novolance);

    }


    @Test
    public void validarMaiorLance() {

        Usuario anderson = new Usuario();
        anderson.setId(1);
        anderson.setNome("Anderson");


        Usuario francisca = new Usuario();
        francisca.setId(1);
        francisca.setNome("Francisca");

        List<Lance> lances = new ArrayList<>();

        lances.add(new Lance(anderson, 200));
        lances.add(new Lance(francisca, 400));

        Leilao leilao = new Leilao(lances);

        Leiloeiro leiloeiro = new Leiloeiro("Galadeira", leilao);

        Assertions.assertEquals(leiloeiro.retornaMaiorlance(lances), 400);

    }

    @Test
    public void validaSeOlanceEhIgualAZero() {

        Usuario anderson = new Usuario();
        anderson.setId(1);
        anderson.setNome("Anderson");


        Usuario francisca = new Usuario();
        francisca.setId(1);
        francisca.setNome("Francisca");
        List<Lance> lances = new ArrayList<>();

        Lance lance = new Lance(anderson, 0);
        Leilao leilao = new Leilao(lances);

        Assertions.assertEquals(leilao.validaLanceZero(lance), true   );

    }

    @Test
    public void verificaSeOlanceEhMenorQueOAnterior() {

        Usuario anderson = new Usuario();
        anderson.setId(1);
        anderson.setNome("Anderson");


        Usuario francisca = new Usuario();
        francisca.setId(1);
        francisca.setNome("Francisca");
        List<Lance> lances = new ArrayList<>();

        Lance lance = new Lance(anderson, 200);

        Lance lance2 = new Lance(anderson, 100);

        Leilao leilao = new Leilao(lances);

        leilao.adicionaNovolance(lance);
        leilao.adicionaNovolance(lance2);



        Assertions.assertEquals(leilao.validarLance(lances), false   );

    }


}
