
import java.util.ArrayList;
import java.util.List;

public class Leilao {


    private Lance lance;

    public double maiorlance = 0;

    public double menorlance;

    public double getMenorlance() {
        return menorlance;
    }

    public void setMenorlance(double menorlance) {
        this.menorlance = menorlance;
    }

    public List<Lance> lanceList = new ArrayList<Lance>();


    public double getMaiorlance() {
        return maiorlance;
    }

    public void setMaiorlance(double maiorlance) {
        this.maiorlance = maiorlance;
    }

    public List<Lance> getLanceList() {
        return lanceList;
    }

    public void setLanceList(List<Lance> lanceList) {
        this.lanceList = lanceList;
    }


    public Lance adicionaNovolance(Lance lance) {

        lanceList.add(lance);

        return lance;

    }

    public Leilao() {

    }

    public Leilao(List<Lance> lanceList) {
        this.lanceList = lanceList;
    }

    public boolean validaLanceZero(Lance lance) {
        if (lance.getValor() <= 0) {

            return true;
        } else {

            return false;
        }


    }


    public boolean validarLance(List<Lance> listadeLances) {

        for (Lance lances : listadeLances) {
            if (lances.getValor() > maiorlance) {

                maiorlance = lances.getValor();
            } else {
                return  false;
            }

        }
        return true;
    }


}
