import java.util.ArrayList;
import java.util.List;

public class Leiloeiro extends Leilao {

    private String nome;
    private Leilao leilao;


    List<Lance> listlance = new ArrayList<Lance>();
    public double maiorlance = 0;


    @Override
    public double getMaiorlance() {
        return maiorlance;
    }

    @Override
    public void setMaiorlance(double maiorlance) {
        this.maiorlance = maiorlance;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }


    public Leiloeiro() {
    }


    public double retornaMaiorlance(List<Lance> listlance) {

        for (Lance lances : listlance) {
            if (lances.getValor() > maiorlance) {

                maiorlance = lances.getValor();
            }

        }
        return maiorlance;

    }



}
